package esi.atl.oo_base;

public class Rectangle {
    private Point bl; //bottom left corner
    private Point ur; //upper right corner

    public Rectangle(Point bottomLeft, Point upperRight) {
        if(bottomLeft.getX() >= upperRight.getX() || bottomLeft.getY() >= upperRight.getY())
            throw new IllegalArgumentException(
                     "bottomLeft must be below and on the left of upperRight"
                     +", received (bottomLeft - upperRight): "
                     +bottomLeft+"-"+upperRight);
        this.bl = new Point(bottomLeft);
        this.ur = new Point(upperRight);
        
    }
    public void move(double dx, double dy) {
        bl.move(dx, dy);
        ur.move(dx, dy);
    }
    public double getPerimeter() {
        return 2*((ur.getX()-bl.getX())+(ur.getY()-bl.getY()));
    }
    @Override
    public String toString() {
        return "Rectangle : ["+bl+", "+ur+"]";
    }
}

class TestRectangle {
    /*
    affiche:
    Rectangle : [(0,0, 0,0),(5,0, 3,0)]
    perimeter: 16
    Rectangle : [(2,0, 5,0),(7,0, 8,0)]
    perimeter: 16
    
    -----------------
    Diag d'objets:
    bl : Point
    x:0
    y:0
    
    ur : Point
    x:0
    y:0
    
    r : Rectangle
    bl: bl
    ur: ur
    
    
    */
    public static void main(String args[]) {
        Point bl = new Point(0, 0);
        Point ur = new Point(5, 3);
        Rectangle r = new Rectangle(bl, ur);
        System.out.println(r);
        System.out.println("perimeter: "+r.getPerimeter());
        System.out.println(bl);
        //r.move(2, 5);
        bl.move(10,10); 
        System.out.println(r);
        System.out.println(bl);
        System.out.println("perimeter: "+r.getPerimeter());
    }
}
