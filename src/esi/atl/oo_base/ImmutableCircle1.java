/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.oo_base;

/**
 *
 * @author G39766
 */
final class ImmutableCircle1 {

        private final Point center;
        private final double radius;
        
        public ImmutableCircle1(Point center, double radius){
            this.center = new Point(center);
            this.radius = radius;
        }
        
        
        
@Override
    public String toString() {
        return "Circle : [" + center + ", " + radius + "]";
    }

        
}

class tryImmCircle1{
    public static void main(String[] args) {
        Point p = new Point(5,6);
        ImmutableCircle1 iC1 = new ImmutableCircle1(p,5);
        System.out.println(iC1);
        p.move(5, 10);
        System.out.println(iC1);
    }
}
