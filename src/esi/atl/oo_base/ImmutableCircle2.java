
package esi.atl.oo_base;

/**
 *
 * @author G39766
 */
final class ImmutableCircle2 {

        private final ImmutablePoint center;
        private final double radius;
        
        public ImmutableCircle2(ImmutablePoint center, double radius){
            this.center = center;
            this.radius = radius;
        }
        
        
        
@Override
    public String toString() {
        return "Circle : [" + center + ", " + radius + "]";
    }

        
}

class tryImmCircle2{
    public static void main(String[] args) {
        ImmutablePoint p = new ImmutablePoint(5,6);
        ImmutableCircle2 iC2 = new ImmutableCircle2(p,5);
        System.out.println(iC2);
        p.move(5, 10);
        System.out.println(iC2);
    }
}
