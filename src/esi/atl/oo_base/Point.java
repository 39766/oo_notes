package esi.atl.oo_base;

class Point {
    
    private double x;
    private double y;
    
    public Point() {
        //System.out.println("Test");
        
        this(0,0); //Doit être lancé en premier
    }
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public Point(Point p) {
        this(p.x, p.y);
    }
  
    public double getX() {return x;}
    public double getY() {return y;}
    public void move(double dx, double dy) {
        x += dx;
        y += dy;
    }
    
    
    
    @Override
    public String toString() {
        return "("+x+", "+y+")";
    }
       public void move(int dx, int dy){
        System.out.println("Méthode move (int,int)");
        x+=dx;
        y+=dy;
    }
       /*
       public boolean move(double dx, double dy) {
            x += dx;
            y += dy;
            return true;
        }
       
       Cette méthode est inutilisable car cette méthode existe déjà.
       Les attributs sont identiques. Malgré le type de retour différent.
       La visibilité et type de retour n'interviennent pas dans une surcharge.
       */
}

class TestPoint {
    /*
         Le programme de test va afficher les coordonées 0,0;0,0 puis 2,0;2,0
    MAJ: Il va maintenant utiliser la méthode ajoutée. (même chose + affiche d'un cout)
    */
    
    public static void main(String args[]) {
        Point p = new Point();
        System.out.println(p);
        p.move(2,2);
        System.out.println(p);
       // System.out.println(p.x);
       // Ne fonctionne pas, attribut privé, il faut utiliser l'accesseur.
        
    }
}

/*
1.2)Une classe publique a besoin de son propre nom de fichier.
1.3)Pas d'erreur

3.2)Aucune, il n'y a pas d'erreur de ne pas créer de constructeur.
    S'il n'y en a pas, un constructeur sans paramètre implicite est utilisé.    

    Dans le cas d'effacement de tout les constructeur, il y a tout de même une
    erreur dans la classe rectangle qui utilise un constructeur à 2param qui
    n'éxiste plus. Ormis cela, cela fonctionne.
    Le programme affichera la même chose qu'auparavant.

    L'utilisation, bien que non conseillé, de valeur explicites d'attributs 
    tels que 10 et 10 , afficherais donc 10,0 10,0 et 12,0 12,0 .



*/